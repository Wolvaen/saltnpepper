/**
 * Created by André Leifeld on 13.05.16.
 */
'use strict';

var jsSHA = require('jssha'),
    md5 = require('md5'),
    r = require('./lib/randString');

function SaltNPepper(passwd, pepper) {
    this.password = passwd;
    this.__pepper = pepper;
};

SaltNPepper.prototype.hash = function(salt) {
    this.flavored = this.salt(this.pepper(this.password), salt);
    return this;
};

SaltNPepper.prototype.matchAgainst = function(hash) {
    return this.flavored === hash;
};

SaltNPepper.prototype.pepper = function(text) {
    var shaObj = new jsSHA("SHA-1", "TEXT");
    shaObj.setHMACKey(this.__pepper, "TEXT");
    shaObj.update(text);
    return shaObj.getHMAC("B64");
};

SaltNPepper.prototype.salt = function(text, salt) {
    return md5(text+salt);
};

SaltNPepper.getRandSalt = function(length) {
    if (length == null) { length = 32; }
    return r.randString(length);
};

module.exports = {
    sNp: SaltNPepper,
    randString: r.randString
};