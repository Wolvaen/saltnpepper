/**
 * Created by André Leifeld on 14.05.16.
 */
'use strict';

var RANDCHARS = '!"§$%&/()=?`´*#\'-_.:,;<>[]|{}≠¡ abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

module.exports = {
    randString: function(length) {
        var i, chars, rstr = '', number = 0.0;

        if (typeof arguments[0] === 'string') {
            chars = arguments[0];
            length = arguments[1];
        } else {
            chars = RANDCHARS;
        }
        for (i = 0; i < length; i++) {
            var number = Math.random();
            if (i % 2 === 0) {
                number = 1-number;
            }
            rstr += chars.substr(Math.round(length*number),1)
        }
        return rstr;
    }
};