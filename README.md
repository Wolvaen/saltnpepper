Generate salted password-hashs and flavor them with some pepper

# Usage:

Example user:

```javascript

    function User() {}
    
    User.prototype.email = null;
    
    User.prototype.password = null;
    
    User.prototype.salt = null;
    
    User.prototype.getFlavoredSalt = function() {
    
        // In this example we use the email address to flavor
        // the salt but you should use your own char-sequenze
        
        return this.email + this.salt;
    }
    
    User.loadByEmail = function(email) {
        // Fetch persisted User by email address
    } 
    
```

Now we want to create a new user:

```javascript

    var SaltNPepper = require('saltnpepper').sNp,

        givenPassword = 'password123',

        serverPepperToken = 'I_AM_GROOT!,
        
        sNp = new SaltNPepper(givenPassword, serverPepperToken),

        john = new User();
        
    john.email = 'john@doe.com';

    john.salt = sNp.getRandSalt();

    john.password = sNp.hash( john.getFlavoredSalt() ).flavored;

```

And now we can simulate a login with false credentials:

```javascript

    var SaltNPepper = require('saltnpepper').sNp,
    
        serverPepperToken = 'I_AM_GROOT!,
        
        postedEmail = 'john@doe.com',

        postedPassword = 'wrong_password',
        
        user = User.loadByEmail(postedEmail),

        salt = user.getFlavoredSalt(),

        sNp = SaltNPepper(postedPassword, serverPepperToken),
        
        hasAccess = sNp.hash(user.salt).matchAgainst(user.password); // false

```

After that we'll try it with a valid credentials:

```javascript
        
        […]
        
        postedEmail = 'john@doe.com',
        
        postedPassword = 'password123',
        
        […]
        
        hasAccess = sNp.hash(user.salt).matchAgainst(user.password); // true
       

```
